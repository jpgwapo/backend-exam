<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use DateTimeInterface;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'image'      => $this->image,
            'title'      => $this->title,
            'content'    => $this->content,
            'slug'       => $this->slug,
            'user_id'    => $this->user_id,
            'created_at' => $this->serializeDate($this->created_at),
            'updated_at' => $this->serializeDate($this->updated_at),
        ];
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
