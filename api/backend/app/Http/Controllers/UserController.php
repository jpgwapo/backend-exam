<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Validation\ValidationException;


class UserController extends Controller
{
    public function register(Request $request)
    {
    	$request->validate([
            'name'                  => 'required|max:20',
            'email'                 => 'required|email|max:25|unique:App\Models\User',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required:password|same:password',
		]);

    	$name = $request->name;
        $email = $request->email;
        $password = Hash::make($request->password);

    	$res = User::create([
			'name' 		=> $name,
			'email' 	=> $email,
			'password' 	=> $password,
		]);

        return response()->json([
            'id' => $res->id
        ]);
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only(['email', 'password']);
        $token = auth('api')->attempt($credentials);

        if (!$token) {
            throw ValidationException::withMessages([
                'error' => ['Incorrect USER ID / Password.'],
            ]);    
        }else{
            return $this->createNewToken($token);
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }
}
