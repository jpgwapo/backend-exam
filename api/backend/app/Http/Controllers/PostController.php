<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\Post as PostResource;
use Illuminate\Support\Str;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return PostResource::collection($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|',
            'title' => 'required|',
            'content' => 'required|',
        ]);

        $image = $request->image;
        $title = $request->title;
        $content = $request->content;
        $slug = Str::of($title)->slug('-');

        try {
            $res = Post::create([
                'image'   => $image,
                'title'   => $title,
                'content' => $content,
                'slug'    => $slug,
                'user_id' => auth('api')->user()->id,
            ]);

            return new PostResource($res);
        } catch (Throwable $e) {
            report($e);

            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($post)
    {
        $res = Post::where('slug', $post)->first();

        if ($res) {
            return new PostResource($res);
        }else{
            return response()->json(array('error' => 'Empty data'), 404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $id = $request->id;
        $image = $request->image;
        $title = $request->title;
        $content = $request->content;
        $slug = Str::of($title)->slug('-');

        try {
            $res = Post::where('slug', $slug)->update([
                        'image'   => $image,
                        'title'   => $title,
                        'content' => $content,
                        'slug'    => $slug,
                        'user_id' => auth('api')->user()->id,
            ]);

            return new PostResource(Post::find($id));
        } catch (Throwable $e) {
            report($e);

            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
