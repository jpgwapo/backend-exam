<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('guest:api')->group(function () {
	Route::post('register', [UserController::class, 'register']);
	Route::post('login', [UserController::class, 'login']);

	Route::get('/posts', [PostController::class, 'index']);
	Route::get('/posts/{id}', [PostController::class, 'show']);

	Route::get('posts/{slug}/comments',[CommentController::class, 'index']);

});

Route::middleware('auth:api')->group(function () {
	Route::post('logout', [UserController::class, 'logout']);

    Route::post('/posts', [PostController::class, 'store']);
    Route::patch('/posts/{id}', [PostController::class, 'update']);

    Route::post('posts/{slug}/comments', [CommentController::class, 'store']);
});
